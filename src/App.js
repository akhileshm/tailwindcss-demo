import React from 'react';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Box from './components/Box';
import Card from './components/Card';
import Form from './components/Form';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

const App = () => (
  <Router>
  	<div>
	    <Navbar />
	    <Route path="/" exact component={Home} />
	    <Route path="/box" exact component={Box} />
	    <Route path="/form" exact component={Form} />
	    <Route path="/card" exact component={Card} />
	  </div>
  </Router>
);

export default App;