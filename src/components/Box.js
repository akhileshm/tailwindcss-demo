import React from 'react';
import { Link } from 'react-router-dom';

const Box = () => {
	return (
		<div className="box">
			<div className="boxItem">
				<img src="/images/avatar.jpg" alt="avatar" />
			</div>
			<div className="boxItem">
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			</div>
			<div className="boxItem">
				<img src="/images/avatar.jpg" alt="avatar" />
			</div>
		</div>
	)
}

export default Box;