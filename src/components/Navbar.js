import React from 'react';
import { Link } from 'react-router-dom';
import './styles.css';

const Navbar = () => {
	return (
		<nav className="navbar">
      <Link to="/">Home</Link>
			<Link to="/box">Box</Link>
			<Link to="/card">Card</Link>
			<Link to="/form">Form</Link>
		</nav>
	)
}

export default Navbar;