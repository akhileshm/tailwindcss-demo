import React from 'react';

const Card = () => (
  <div class="cards">
	  <div class="card">
	  	<div className="cardContent">
	    	<img class="block h-16 sm:h-24" src="/images/avatar.jpg" alt="avatar" />
	    </div>
	    <div class="cardContent">
	      <p class="">Erin Lindford</p>
	      <p class="text-gray-600">Product Engineer</p>
	      <div class="mt-4">
	        <button class="btn btn-blue">Message</button>
	      </div>
	    </div>
	  </div>
	  <div class="card">
	  	<div className="cardContent">
	    	<img class="block h-16 sm:h-24" src="/images/avatar.jpg" alt="avatar" />
	    </div>
	    <div class="cardContent">
	      <p class="">Erin Lindford</p>
	      <p class="text-gray-600">Product Engineer</p>
	      <div class="mt-4">
	        <button class="btn btn-blue">Message</button>
	      </div>
	    </div>
	  </div>
	  <div class="card">
	  	<div className="cardContent">
	    	<img class="block h-16 sm:h-24" src="/images/avatar.jpg" alt="avatar" />
	    </div>
	    <div class="cardContent">
	      <p class="">Erin Lindford</p>
	      <p class="text-gray-600">Product Engineer</p>
	      <div class="mt-4">
	        <button class="btn btn-blue">Message</button>
	      </div>
	    </div>
	  </div>
	</div>
);

export default Card;