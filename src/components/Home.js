import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
	return (
		<div className="container">
      <h1 className="heading">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      </h1>
      <h2 className="subHeading">
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      </h2>
      <p className="para">
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			</p>
      <h1 className="heading">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      </h1>
      <h2 className="subHeading">
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      </h2>
      <p className="para">
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			</p>
	  	<div>
		    <button className="btn">button</button>
		    <button className="btn btn-blue">button</button>
		    <button className="btn btn-red">button</button>
		  </div>
	  </div>
	)
}

export default Home